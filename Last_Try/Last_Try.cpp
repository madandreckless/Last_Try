// Last_Try.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <bitset>
#include <cmath>

int main() //точка входа main

{

	std::cout << "Hello, world\n";
	std::cout << "sizeof(int)" << sizeof(int) << std::endl;
	//объект cout вместе с оператором << позволяет выводить как и строки и так переменые других типов,
	//не заботясь о преобразовании в строку
	//иными словами делается приведение типов
	//Шаблон endl в конечном счете представляет то же самый символ \n
	// в некоторых случаех приходится писать \r \n
	std::cout << "sizeof(short)=" << sizeof(short) << std::endl;
	std::cout << "sizeof(char)=" << sizeof(char) << std::endl;
	std::cout << "sizeof(float)=" << sizeof(float) << std::endl;
	std::cout << "sizeof(double)=" << sizeof(double) << std::endl;
	std::cout << "sizeof(long int)=" << sizeof(long int) << std::endl;
	std::cout << "sizeof(long)=" << sizeof(long) << std::endl;
	std::cout << "sizeof(long long)=" << sizeof(long long) << std::endl;
	std::cout << "sizeof(long long int)=" << sizeof(long long int) << std::endl;
	std::cout << "sizeof(true)=" << sizeof(true) << std::endl;
	std::cout << "sizeof(false)=" << sizeof(false) << std::endl;
	std::cout << "sizeof(bool)=" << sizeof(bool) << std::endl;
	int a = INT_MAX; //1 объявление пременной integer
					 //2 инициализация
	std::cout << "a = " << std::bitset <32>(a) << std::endl;
	int b = 0b10000000000000000000000000000000;
	unsigned int b_without_sign = 0b10000000000000000000000000000000;
	int oct = 0100;
	int hex = 0xff;
	std::cout << "b =" << b << std::endl;
	std::cout << "b_without_sign =" << b_without_sign << std::endl;
	std::cout << "oct =" << oct << std::endl;
	std::cout << "hex =" << hex << std::endl;




	getchar(); //ожидание ввода любого символа
	return 0; // возрат из функции main означает завершение работы приложения

}

