// Stroky.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include <bitset>
using namespace std;

//СТРОКИ

//строки старого С-типа представляют из себя массив символов с нулевым окончанием

char cstr1[] = "C - style string 1";
char cstr2[] = { 'C', '-', 's', 't', 'y', 'l', 'e', ' ', 's', 't', 'r', 'i', 'n', 'g', '2', '\0' };

//функции для работы с С-строками
//вывод на печать printf(char[], , ,)
//с помощью printf можно не просто печатать готовую строку
//но ипеременные, попутно преобразовывая их
//в символьный вид и применя форматирование
//форматирование printf("базовая % sстрока%f", параметр1, параметр2) 

//позиции куда будут подставляться параметры, обозначаются в базовой строке
// символом "%"

// значение  символов формотирования берётся из таблицы справки http://www.cplusplus.com/reference/cstdio/printf/
//значение некоторых из них
// %f - параметр подставляется на метсо %f, оформленное в виде числа с плавоющей точкой
// %s- параметр подставляется на метсо %s как строка
// %d - параметр подставляется на метсо %d как целое число
// %e - параметр подставляется на метсо %e как число в экпоненциальном виде


//объединение строк strcat(*char, *char)
//копирование из одной строки в другую strcpy()
//сравенение strcmp ()
//длина строки strlen()

std::string cppstr1 = "C++ - Class string 1";
std::string cppstr2 = "C++ - Class string 2";

int main()
{

	printf(cstr1);

	//в функцию передаётся массив char (а имя массива является Указателем на самый первый элемент массива) 
	//либо указатель *char
	printf("\n\n");

	printf("Insert double:%f, insert long long: %d, \n"
		"insert string: %s, \n"
		"insert exponential: %e, \n"//дробное число между >=1 и <2. умножаемое на 10
		"insert double with pricision: %10.2f\n"//10.2 - десять пустых мест до запятой и 2 знака после запятой
		"insert long long as double: %10.2f\n" //ОШИБКА неверное приведение типов
		"insert long long as int: %010d\n", //ОШИБКА неверное приведение типов
		1000.15,
		132456789,
		"string =) ",
		1123456.1123456,
		1123456.1123456,
		(long long)10000,
		10.5);



	//копирование из одной строки в другую 
	//cstr3 = strcpy(cstr1, cstr2);// требуется использование более сложного безопасного аналога strcpy_s()
	//printf_s("Result of strcpy(%s, %s) = %s\n", cstr1, cstr2, cstr3);



	//сравнение 
	int i = strcmp(cstr1, cstr2);
	printf_s("Result of strcmp(%s, %s) = %d\n", cstr1, cstr2, i);
	cout << "Instead of strcmp() use (cppstr1 == cppstr2) = " << (cppstr1 == cppstr2) << endl << endl;

	//длина строки 
	int lngth = strlen(cstr1);
	printf_s("Result of strlen(%s) = %d\n", cstr1, lngth);
	cout << "Instead of strlen() use std::string.lenght() = " << cppstr1.length() << endl << endl;

	//копированние
	char cstr3[255];
	strcpy_s(cstr3, 255, cstr2);// требуется использование более сложного безопасного аналога strcpy_s()
	printf_s("Result of strcpy(%s) = %s\n", cstr3, cstr2);
	cppstr2 = cppstr1;
	cout << "Instead of strcpy() use operator =:\t " << cppstr2 << endl << endl;

	//объединение строк 
	strcat_s(cstr3, 255, cstr2);
	cout << "(Result of strcat (cstr1, cstr2);" << cstr3 << endl;
	cout << "Instead of strcat() use operator +:\t" << (cppstr1 + cppstr2) << endl << endl;

	/*
	в стандартной библиотеке C++ массив символов инкапсулированн в класс в который так  же собраны самые
	часто используемы методы обработки и для которого перегруженны операторы
	*/


	//std::fstream - универсальный класс для ввода (чтение из файла) и вывода (запись в файл)
	//std::ifstream - универсальный класс для ввода(чтение из файла) 
	//std::ofstream - универсальный класс для вывода (запись в файл)
	//аналогично cout - потоку, предназначенному для вывода на консоль
	//fstream являеется потоком, предназначенным для ввода/вывода из/в файл


	fstream fs;
	fs.open("Some text file. txt",//при открытии файла необходимо указать имя файла
		std::fstream::in
		| std::fstream::out
		/*| std::fstream::app *///app = append добавляется режим с записью в конец файла
		| std::fstream::trunc);//trunc = trunced - режим с перерзаписью всего файла
							   // а также режим открытия: с перезаписью или без, для чтения/
							   // чтобы не вникать в типы стандартной библиотеки используется тип auto:
							   // в зависимости от правой части выражения компилятор сам подбирает тип
	auto fstream_mode = std::fstream::in | std::fstream::out | std::fstream::app;
	cout << "std::fstream::in | std::fstream::out | std::fstream::app = " << std::bitset<8>(fstream_mode) << endl;
	fs << 1000.15 << endl
		<< 132456789 << endl
		<< "string =)" << endl
		<< 1123456.1123456 << endl
		<< 1.1123456 << endl
		<< (long long)10000 << endl
		<< 10.5 << endl;
	fs.close();




	getchar();
	return 0;
}