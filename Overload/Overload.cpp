// Overload.cpp : Defines the entry point for the console application.
//Перегрузка функций методов и операторов

#include "stdafx.h"
#include <iostream>

using namespace std;



// ПЕРЕГРУЗКА ФУНКЦИЙ, МЕТОДОВ И ОПЕРАТОРОВ
// Перегрузка - объявление нескольких фукнций (а также методов классов и операторов, которые тоже
// явяются функциями) с одинаковыми именами, но разным набором аргументов.
int fnc1(double p1, char p2)
{
	cout << "fnc1(double, char) " << endl;
	return p1 + p2;
}

/*также замена функций по одному выходному параметру на является перегрузкой
double fnc1(double p1, char p2)
{
cout << "fnc1(double, char) " << endl;
return p1 + p2;
}
*/
int fnc1(int p1, short p2)
{
	cout << "fnc1(int, short) " << endl;
	return p1 + p2;
}

/*для компилятора критерием является сигнатура функции - имя + последовательностьаргуметов + их типы
даже если аргуметы имеют разные названия, но типы и последовательность одинаковые компилятор считает такой случай не перегрузкой,
а повторным объявлением, что является ошибкой
int fnc1(int p1, short p2)
{
cout << "fnc1(int, short) " << endl;
return p1 - p2;
}
*/

int fnc1(long long p1, long long p2)
{
	cout << "fnc1(long long, long long) " << endl;
	return p1 + p2;
}


class sample_class
{
public:
	sample_class()
	{

	}

	int property1;
	double property2;

	int fnc1(double p1, char p2)
	{
		cout << "sample_class::fnc1(double, char) " << endl;
		return p1 + p2;
	}

	int fnc1(int p1, short p2)
	{
		cout << "sample_class::fnc1(int, short) " << endl;
		return p1 + p2;
	}


	~sample_class()
	{

	}

	//бинарные операторы прописываются вне класса, так как результат не обязательно 
	//записывается в один из операндов, а может записываться некий третий объект
	//результат зависывается в тот же самый объект
	//для которого овызван оператор, поэтому унарный оператор стоит отнетси
	//к методам класса

	sample_class & operator += (const sample_class & operand)
		//здесь const - "защита от забывчивости" не даёт изменить внутри метода операнд,
		// который по логике и не должен меняться
	{
		property1 = property1 + operand.property1;
		this->property2 = this->property2 + operand.property2;//this - эффект не менятеся
		return (*this);
	}


	sample_class & operator -= (const sample_class & operand);

};

sample_class & sample_class::operator-=(const sample_class & operand)
{
	property1 = property1 - operand.property1;
	this->property2 = this->property2 - operand.property2;//this - эффект не менятеся
	return (*this);
}

sample_class operator  + (const sample_class & lhs,//left hand side
	const sample_class & rhs)//right hand side
{
	sample_class result;//создать новый объект для записи результатов

						//сообственно, сложение
	result.property1 = lhs.property1 + rhs.property1;
	result.property2 = lhs.property2 + rhs.property2;

	return result;
}


ostream & operator << (ostream & os,//левосторонний оператор, cout
	sample_class & rhs)// правосторонний оператор, сообственно, то, что вывыодится
{
	os << "sample_class:" << endl;
	os << "\tproperty1 = " << rhs.property1 << ";" << endl;
	os << "\tproperty2 = " << rhs.property2 << ";" << endl;

	return os;
}

//Операторы перегружаются для пользовательских типов, для нестандартных, классов для которых сам компилятор "не знает", какие действия применять
//в этом случае пользователь сам прописывает требуемые от оператора действия



int main() //Функция MAIN не может быть перегружена!!!!
{
	double a = 10.5;
	char b = 15;
	int c = 1000;
	short d = 101325;
	long long e = 1;

	fnc1(a, b);
	fnc1(e, e);
	fnc1(c, d);

	sample_class obj1, obj2;
	obj1.fnc1(a, b);
	obj1.fnc1(c, d);

	obj1.property1 = 11;
	obj1.property2 = 10.5;
	obj2.property1 = 111;
	obj2.property2 = 101.5;

	//obj1 = obj1 * obj2 поначалу компилятор не знает, что делать при вызове оператора *


	//применение и работа оператора "+="
	cout << "*** before use of += " << endl;
	cout << " obj1.property1 = " << obj1.property1 << endl;
	cout << " obj1.property2 = " << obj1.property2 << endl;
	obj1 += obj2;
	cout << "*** after use of += " << endl;
	cout << " obj1.property1 = " << obj1.property1 << endl;
	cout << " obj1.property2 = " << obj1.property2 << endl;



	getchar();
	return 0;
}

/*
через стек, доп параметр стека
*/



/*
-Перегруженные имена имеют одинаковые имена на уровне исходных кодов, для компилятора и в бинарнике - это разные функции с разными адресами
-компилятор принимает решение какую из перегрузок вызывать на месте при вызове по составу и типу параметро которые передал программист
-Служебное слово operator
- унарные операторы пергружаются внутри класса, бинарные снаружи в виде отдельных функций


*/

/*
ДЗ сложение и умножение для матриц и векторов
для векторов и матриц определить
операторы "+" x2, "-" x2, "*" x3, "<<"
*/

/*
Высокоуровневая работа компилятора C++:
1. Прописывание кода по передаче параметров функций в стек перед вызовом
и очистка стека перед вызовом
2. Автоматический вызов конструктора и деструктора
3. Передача указателя для this внутри методов классов
*/