#pragma once




class Matrice
{

protected:
	int ** Arr;
	int nRow;
	int nCol;

public:
	
	Matrice();
	Matrice(int row, int col);
	int ** Mirror_Arr;
	void fill_arr();
	void Arr_out();
	void Arr_out_for_skal();
	void add_matr_or_vec(int **& matr_a, int **& matr_b);
	void minus(int **& matr_a, int **& matr_b);
	void mult_arr(int ** &matr_a, int ** &matr_b, int row_a, int col_a, int col_b);
	void mult_vec_and_matr_OR_skal_vec(int ** &vec_a, int ** &matr_b, int row, int col);

	void delete_array();

	~Matrice();

};

class Vector : public Matrice
{
public:
	Vector() : Matrice() {};
	Vector(int col) : Matrice (1, col) {};
	void vec_multiplication_vec(int ** &v_a, int ** &v_b);
};


