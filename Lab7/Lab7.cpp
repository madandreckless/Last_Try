// Lab7.cpp : Defines the entry point for the console application.
//Динамические массивы

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include "Array.h"

using namespace std;
int select_funct;
int nRow_a;
int nCol_a;
int nRow_b;
int nCol_b;
const int nRow_Vec = 1;

ostream & operator << (ostream & os,//левосторонний оператор, cout
	Matrice & Arr)// правосторонний оператор, сообственно, то, что вывыодится
{

	Arr.Arr_out();

	return os;
}

ostream & operator << (ostream & os,//левосторонний оператор, cout
	Vector & Arr)// правосторонний оператор, сообственно, то, что вывыодится
{

	Arr.Arr_out();

	return os;
}

Matrice operator + (Matrice &matr_a, Matrice &matr_b)
{
	Matrice Matrice_Add(nRow_a, nCol_b);
	if (nRow_a == nRow_b & nCol_a == nCol_b)
	{
		Matrice_Add.add_matr_or_vec(matr_a.Mirror_Arr, matr_b.Mirror_Arr);
		cout << Matrice_Add << endl;
	}
	else
	{
		cout << "ERROR" << endl;
	}

	Matrice_Add.delete_array();

	return Matrice_Add;
}

Vector operator + (Vector &Vec_a, Vector &Vec_b)
{
	Vector Vector_Add(nCol_b);
	if (nCol_a == nCol_b)
	{
		Vector_Add.add_matr_or_vec(Vec_a.Mirror_Arr, Vec_b.Mirror_Arr);
		cout << Vector_Add << endl;
	}
	else
	{
		cout << "ERROR" << endl;
	}

	Vector_Add.delete_array();


	return Vector_Add;
}

Matrice operator - (Matrice &matr_a, Matrice &matr_b)
{
	Matrice Matrice_Minus(nRow_a, nCol_b);
	if (nRow_a == nRow_b & nCol_a == nCol_b)
	{
		Matrice_Minus.minus(matr_a.Mirror_Arr, matr_b.Mirror_Arr);
		cout << Matrice_Minus << endl;
	}
	else
	{
		cout << "ERROR" << endl;
	}

	Matrice_Minus.delete_array();

	return Matrice_Minus;
}

Vector operator - (Vector &Vec_a, Vector &Vec_b)
{
	Vector Vector_Minus(nCol_b);
	if (nRow_a == nRow_b & nCol_a == nCol_b)
	{
		Vector_Minus.minus(Vec_a.Mirror_Arr, Vec_b.Mirror_Arr);
		cout << Vector_Minus << endl;
	}
	else
	{
		cout << "ERROR" << endl;
	}

	Vector_Minus.delete_array();

	return Vector_Minus;
}

Matrice operator * (Matrice &Matr_a, Matrice &Matr_b)
{

	Matrice Matrice_Mult(nRow_a, nCol_b);
	if (nRow_b == nCol_a)
	{
		Matrice_Mult.mult_arr(Matr_a.Mirror_Arr, Matr_b.Mirror_Arr, nRow_a, nCol_a, nCol_b);
		cout << Matrice_Mult << endl;
	}
	else
	{
		cout << "ERROR" << endl;
	}

	Matrice_Mult.delete_array();

	return Matrice_Mult;
}

Vector operator * (Vector &Vec_a, Vector &Vec_b)
{
	Vector Vector_Mult(nCol_b);
	if (nCol_a == nCol_b)
	{
		Vector_Mult.mult_vec_and_matr_OR_skal_vec(Vec_a.Mirror_Arr, Vec_b.Mirror_Arr, 1, nCol_b);
		Vector_Mult.Arr_out_for_skal();
	}
	Vector_Mult.delete_array();

	return Vector_Mult;

}

Matrice operator * (Matrice &Matr_b, Vector &Vec_a)
{
	Matrice Matrice_Multi(nRow_b, nCol_b);
	if (nCol_a == nCol_b)
	{
		Matrice_Multi.mult_vec_and_matr_OR_skal_vec(Vec_a.Mirror_Arr, Matr_b.Mirror_Arr, nRow_b, nCol_b);
		Matrice_Multi.Arr_out_for_skal();
	}
	Matrice_Multi.delete_array();

	return Matrice_Multi;
}

int main()
{

	do
	{
		std::cout << "Press 1 - Addition of Matrices" << std::endl;
		std::cout << "Press 2 - Minus of Matrices" << std::endl;
		std::cout << "Press 3 - Addition of Vectors" << std::endl;
		std::cout << "Press 4 - Vectors of Vectors" << std::endl;
		std::cout << "Press 5 - Multiplication of Matrices" << std::endl;
		std::cout << "Press 6 - Multiplication of Matrice and Vector" << std::endl;
		std::cout << "Press 7 - Skalyar multiplication of Vectors" << std::endl;
		std::cout << "Press 8 - Vector multiplication of Vectors" << std::endl;
		std::cout << "Press other - Exit" << std::endl;
		std::cout << "Enter....";

		std::cin >> select_funct;

		switch (select_funct)
		{
		case 1:
		{


			cout << "Addition of matrices" << endl;
			cout << "Folding can be a matrix the same size the result is a matrix of the same size" << std::endl;
			cout << "Enter the size of the Matr_A(Row and Column)" << endl;
			cin >> nRow_a >> nCol_a;
			Matrice Matrice_A(nRow_a, nCol_a);
			cout << "Matrice A =" << endl;
			Matrice_A.fill_arr();
			cout << Matrice_A << endl;
			cout << "Enter the size of the Matr_B(Row and Column)" << endl;
			cin >> nRow_b >> nCol_b;
			Matrice Matrice_B(nRow_b, nCol_b);
			cout << "Matrice B =" << endl;
			Matrice_B.fill_arr();
			cout << Matrice_B << endl;


			cout << "Matrice_Add = Matrice_A + Matrice B =" << endl;
			Matrice_A + Matrice_B;
			Matrice_A.delete_array();
			Matrice_B.delete_array();


			break;
		}
		case 2:
		{
			cout << "Minus of matrices" << endl;
			cout << "Folding can be a matrix the same size the result is a matrix of the same size" << std::endl;
			cout << "Enter the size of the Matr_A(Row and Column)" << endl;
			cin >> nRow_a >> nCol_a;
			Matrice Matrice_A(nRow_a, nCol_a);
			cout << "Matrice A =" << endl;
			Matrice_A.fill_arr();
			cout << Matrice_A << endl;
			cout << "Enter the size of the Matr_B(Row and Column)" << endl;
			cin >> nRow_b >> nCol_b;
			Matrice Matrice_B(nRow_b, nCol_b);
			cout << "Matrice B =" << endl;
			Matrice_B.fill_arr();
			cout << Matrice_B << endl;


			cout << "Matrice_Add = Matrice_A - Matrice B =" << endl;
			Matrice_A - Matrice_B;
			Matrice_A.delete_array();
			Matrice_B.delete_array();


			break;
		}
		case 3:
		{
			cout << "Addition of Vectors" << endl;
			cout << "Size Vector A must be equal to size Vector B " << endl;
			cout << "Enter the size of the Vector A" << endl;
			cin >> nCol_a;
			Vector Vector_A(nCol_a);
			cout << "Vector A =" << endl;
			Vector_A.fill_arr();
			cout << Vector_A << endl;
			cout << "Enter the size of the Vector B" << endl;
			cin >> nCol_b;
			Vector Vector_B(nCol_b);
			std::cout << "Vector B =" << endl;
			Vector_B.fill_arr();
			cout << Vector_B << endl;
			if (nCol_a == nCol_b)

				cout << "Vector_Add = Vector_A + Vector_B =" << endl;
			Vector_A + Vector_B;

			Vector_A.delete_array();
			Vector_B.delete_array();



			break;
		}

		case 4:
		{
			cout << "Minus of Vectors" << endl;
			cout << "Size Vector A must be equal to size Vector B " << endl;
			cout << "Enter the size of the Vector A" << endl;
			cin >> nCol_a;
			Vector Vector_A(nCol_a);
			cout << "Vector A =" << endl;
			Vector_A.fill_arr();
			cout << Vector_A << endl;
			cout << "Enter the size of the Vector B" << endl;
			cin >> nCol_b;
			Vector Vector_B(nCol_b);
			std::cout << "Vector B =" << endl;
			Vector_B.fill_arr();
			cout << Vector_B << endl;
			if (nCol_a == nCol_b)

				cout << "Vector_Add = Vector_A - Vector_B =" << endl;
			Vector_A - Vector_B;

			Vector_A.delete_array();
			Vector_B.delete_array();

			break;
		}

		case 5:
		{
			std::cout << "Multiplication of Matrices" << std::endl;
			std::cout << "Two matrices can be multiplied if number of columns of the first matrix equals the number of rows of the second matrix." << std::endl;
			std::cout << "Enter the size of the Matr_A(Row and Column)" << std::endl;
			std::cin >> nRow_a >> nCol_a;
			Matrice Matrice_A(nRow_a, nCol_a);
			std::cout << "Matrice A =" << std::endl;
			Matrice_A.fill_arr();
			cout << Matrice_A << endl;
			std::cout << "Enter the size of the Matr_B(Row and Column)" << std::endl;
			std::cin >> nRow_b >> nCol_b;
			Matrice Matrice_B(nRow_b, nCol_b);
			std::cout << "Matrice B =" << std::endl;
			Matrice_B.fill_arr();
			cout << Matrice_B << endl;

			std::cout << "Matrice_C = Matrice_A * Matrice B =" << std::endl;
			Matrice_A * Matrice_B;

			Matrice_A.delete_array();
			Matrice_B.delete_array();


			break;
		}

		case 6:
		{
			std::cout << "Multiplication of matrice and vector" << std::endl;
			std::cout << "When multiplying a matrix by a vector-row the number of columns in the matrix must match the number of columns in the vector-row " << std::endl;
			std::cout << "Enter the size of the Matrice" << std::endl;
			std::cin >> nRow_b >> nCol_b;
			Matrice Matrice_B(nRow_b, nCol_b);
			std::cout << "Matrice B =" << std::endl;
			Matrice_B.fill_arr();
			cout << Matrice_B << endl;
			std::cout << "Enter the size of the Vector" << std::endl;
			std::cin >> nCol_a;
			Vector Vector_A(nCol_a);
			std::cout << "Vector =" << std::endl;
			Vector_A.fill_arr();
			cout << Vector_A << endl;

			std::cout << "Matrice_Rez = Matrice * Vector =" << std::endl;
			Matrice_B * Vector_A;


			break;
		}
		case 7:
		{
			std::cout << "Vector multiplication of vectors" << std::endl;
			std::cout << "Size Vector A must be equal to size Vector B " << std::endl;
			std::cout << "Enter the size of the Vector A" << std::endl;
			std::cin >> nCol_a;
			Vector  Vector_A(nCol_a);
			std::cout << "Vector A =" << std::endl;
			Vector_A.fill_arr();
			cout << Vector_A << endl;
			std::cout << "Enter the size of the Vector B" << std::endl;
			std::cin >> nCol_b;
			Vector Vector_B(nCol_b);
			std::cout << "Vector B =" << std::endl;
			Vector_B.fill_arr();
			cout << Vector_B << endl;


			std::cout << "Vector_C = Vector_A * Vector_B =" << std::endl;
			Vector_A * Vector_B;

			Vector_A.delete_array();
			Vector_B.delete_array();

			break;
		}

		case 8:
		{
			std::cout << "Skalyar multiplication of vectors" << std::endl;
			Vector Vector_A(3);
			std::cout << "Vector A =" << std::endl;
			Vector_A.fill_arr();
			cout << Vector_A << endl;
			Vector Vector_B(3);
			std::cout << "Vector_B =" << std::endl;
			Vector_B.fill_arr();
			cout << Vector_B << endl;
			Vector * Vector_Rez = new Vector(3);
			Vector_Rez->vec_multiplication_vec(Vector_A.Mirror_Arr, Vector_B.Mirror_Arr);
			delete Vector_Rez;
			break;
		}

		default:
		{
			break;
		}

		}
	} while (select_funct >= 1 & select_funct <= 8);



	getchar();

	return 0;
}


